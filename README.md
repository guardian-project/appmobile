# Aplicación móvil del proyecto 9925001 THD 2019 - Guardian

Aplicación móvil del proyecto 9925001 THD 2019 - Guardian

## Preparacion del entorno de desarrollo

```
ionic cordova platform add android
```

## Ejecución para pruebas

```
ionic serve
```

## Compilación para Android

```
ionic cordova build android
```

## Ejecución / Despliegue para Android

```
ionic cordova run android
```
